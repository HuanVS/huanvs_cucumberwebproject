package steps;

import core.BaseTests;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.ProductPage;

import java.util.List;
import java.util.Map;

public class ProductSteps extends BaseTests {
    ProductPage productPage = new ProductPage(getDriver());

    @When("Click to the Catalog on the left menu.")
    public void clickToTheCatalogOnTheLeftMenu() {
        productPage.clickCatalogItem();
    }

    @And("Click to the Products")
    public void clickToTheProducts() {
        productPage.clickProductItem();
    }

    @Then("The product page should be displayed with some filed")
    public void theProductPageShouldBeDisplayedWithSomeFiled() {
        productPage.verifyViewDisplay();
        productPage.verifyColumnDisplay();
        productPage.verifyTableListProduct();
        productPage.verifyFilterBtnDisplay();
        productPage.verifySearchKeyDisplay();
    }

    @And("Input search key and click search")
    public void inputSearchKeyAndClickSearch(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        productPage.inputTextToSearchKey(data.get(0).get("searchKey"));
    }

    @Then("The search result display product with name")
    public void theSearchResultDisplayProductWithName(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        productPage.verifyNameOfItem(data.get(0).get("name"));
    }

}
