package steps;

import core.BaseTests;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.CustomerPage;

import java.util.List;
import java.util.Map;

public class CustomerSteps extends BaseTests {

    CustomerPage customerPage = new CustomerPage(getDriver());

    @When("Click to the Customers on the left menu")
    public void clickToTheCustomersOnTheLeftMenu() {
        customerPage.clickCustomerMenuItem();
    }

    @And("Click to the All Customers")
    public void clickToTheAllCustomers() {
        customerPage.clickAllCustomerItem();
    }

    @And("Click Add New Customer")
    public void clickAddNewCustomer() {
        customerPage.clickAddNewCustomer();
    }

    @And("Input the requite field")
    public void inputTheRequiteField(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        customerPage.inputInfoNewCustomer(data.get(0).get("firstname"), data.get(0).get("lastname"), data.get(0).get("email"));
    }

    @And("Click to the Save Customer")
    public void clickToTheSaveCustomer() {
        customerPage.clickSaveBtn();
    }

    @Then("The message should be displayed")
    public void theMessageShouldBeDisplayed(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        customerPage.verifySuccessMsg(data.get(0).get("msg"));
    }
}
