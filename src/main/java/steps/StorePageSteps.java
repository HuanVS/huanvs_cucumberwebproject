package steps;

import core.BaseTests;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;
import pages.StorePage;

import java.util.List;
import java.util.Map;

public class StorePageSteps extends BaseTests {

    StorePage storePage = new StorePage(getDriver());

    @And("Click to the Create Store.")
    public void clickToTheCreateStore() {
        storePage.clickAddStore();
    }

    @And("Input : Web Site, Name, Code and Root Category")
    public void inputWebSiteNameCodeAndRootCategory(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        storePage.inputInformation(data.get(0).get("Website"),data.get(0).get("name"),data.get(0).get("code"),data.get(0).get("category"));
    }

    @And("Click Save")
    public void clickSave() {
        storePage.clickSaveBtn();
    }

    @Then("Verify message should be displayed")
    public void verifyMessageShouldBeDisplayed(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        storePage.verifyMsg(data.get(0).get("msg"));
    }
}
