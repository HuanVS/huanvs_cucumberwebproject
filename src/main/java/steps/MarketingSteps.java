package steps;

import core.BaseTests;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.CatalogPage;
import pages.HomePage;
import pages.MarketingPage;

import java.util.List;
import java.util.Map;

public class MarketingSteps extends BaseTests {
    MarketingPage marketingPage = new MarketingPage(getDriver());
    HomePage homePage = new HomePage(getDriver());

    @When("Click to the Marketing and choose Catalog Price Rule on the left menu.")
    public void clickToTheMarketingAndChooseCatalogPriceRuleOnTheLeftMenu() {
        homePage.clickMarketingMenuItem();
        homePage.clickCatalogPriceRule();
    }

    @And("Click to Add New Rule")
    public void clickToAddNewRule() {
        marketingPage.clickAddRuleBtn();
    }

    @And("Click Save button")
    public void clickSaveButton() {
        marketingPage.clickSaveBtn();
    }

    @Then("The message for each required field should be displayed:")
    public void theMessageForEachRequiredFieldShouldBeDisplayed(DataTable table) {
        List<Map<String,String>> data=table.asMaps(String.class,String.class);
        marketingPage.verifyErrorMsgDisplay(data.get(0).get("msg"));
    }

}
