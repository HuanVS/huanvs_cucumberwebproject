package pages;

import core.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id="menu-magento-catalog-catalog")
    private WebElement catalogItem;

    @FindBy(xpath = "//a/span[text()='Products']")
    private WebElement productItem;

    @FindBy(id="add_new_product-button")
    private WebElement addProduct;

    @FindBy(xpath="(//input[@id='fulltext'])[1]")
    private WebElement searchBox;

    @FindBy(xpath ="//button[text()='Filters']")
    private WebElement filterBtn;

    @FindBy(xpath = "(//button[@data-bind='toggleCollapsible'])[2]")
    private WebElement view;

    @FindBy(xpath = "(//span[text()='Columns'])[1]")
    private WebElement column;

    @FindBy(xpath = "(//table[@class='data-grid data-grid-draggable'])[2]")
    private WebElement tableListProduct;

    @FindBy(xpath = "(//table[@class='data-grid data-grid-draggable'])[2]//div[text()='Joust Duffle Bag']")
    private WebElement nameItemInTable;

    public void clickCatalogItem(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(catalogItem));
        catalogItem.click();
    }

    public void clickProductItem(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(productItem));
        productItem.click();
    }

    public boolean verifySearchKeyDisplay(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(searchBox));
        return searchBox.isDisplayed();
    }

    public boolean verifyFilterBtnDisplay(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(filterBtn));
        return filterBtn.isDisplayed();
    }

    public boolean verifyViewDisplay(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(view));
        return view.isDisplayed();
    }

    public boolean verifyColumnDisplay(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(column));
        return column.isDisplayed();
    }

    public boolean verifyTableListProduct(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(tableListProduct));
        return tableListProduct.isDisplayed();
    }

    public void inputTextToSearchKey(String searchKey){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(searchBox));
        searchBox.clear();
        searchBox.sendKeys(searchKey);
        searchBox.sendKeys(Keys.ENTER);
    }

    public boolean verifyNameOfItem(String name){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(nameItemInTable));
        return nameItemInTable.getText().equals(name);
    }
}
