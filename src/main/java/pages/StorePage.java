package pages;

import core.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

public class StorePage extends BasePage {

    public StorePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "add_group")
    private WebElement addStore;

    @FindBy(id = "group_website_id")
    private WebElement website;

    @FindBy(id = "group_name")
    private WebElement groupName;

    @FindBy(id = "group_code")
    private WebElement groupCode;

    @FindBy(id = "group_root_category_id")
    private WebElement groupCategory;

    @FindBy(id = "save")
    private WebElement saveBtn;

    @FindBy(id = "messages")
    private WebElement message;

    public void clickAddStore(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(addStore));
        addStore.click();
    }

    public void inputInformation(String web, String name, String code, String category){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(website));
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(website));
        Select select = new Select(website);
        select.selectByVisibleText(web);

        getWebDriverWait().until(ExpectedConditions.visibilityOf(groupName));
        groupName.sendKeys(name);

        getWebDriverWait().until(ExpectedConditions.visibilityOf(groupCode));
        groupCode.sendKeys(code);

        getWebDriverWait().until(ExpectedConditions.visibilityOf(groupCategory));
        Select select2 = new Select(groupCategory);
        select2.selectByVisibleText(category);
    }

    public void clickSaveBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(saveBtn));
        saveBtn.click();
    }

    public boolean verifyMsg(String msg){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(message));
        return message.getText().equals(msg);
    }
}
