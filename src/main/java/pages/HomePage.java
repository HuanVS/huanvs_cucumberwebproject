package pages;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HomePage extends BasePage {

    @FindBy(id= "username")
    private WebElement username;

    @FindBy (id = "login")
    private WebElement password;

    @FindBy(xpath = "(//span[text()='Back to your orders'])[1]")
    private WebElement backToYourOrdersItem;

    @FindBy(xpath = "//button[@class='action-login action-primary']")
    private WebElement btnSignIn;

    @FindBy(xpath = "//span[@class='spinner']")
    private WebElement loading;

    @FindBy(id = "menu-magento-backend-marketing")
    private WebElement marketingMenuItem;

    @FindBy(xpath = "//span[text()='Catalog Price Rule']")
    private WebElement catalogPriceRule;

    @FindBy(xpath = "//span[text()='Stores']")
    private WebElement storeItem;

    @FindBy(xpath = "//span[text()='Product']")
    private WebElement productInStoreItem;

    @FindBy(xpath = "//li[@class='item-system-store    level-2']")
    private WebElement allStoreItem;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openMagento(String URL){
        getDriver().get(URL);
    }

    public void login(String user,String pass){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(username));
        username.sendKeys(user);
        password.sendKeys(pass);
        btnSignIn.click();
    }

    public void clickMarketingMenuItem(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(marketingMenuItem));
        marketingMenuItem.click();
    }

    public void clickCatalogPriceRule(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(catalogPriceRule));
        catalogPriceRule.click();
    }

    public void clickStoreItem(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(storeItem));
        storeItem.click();
    }

    public void clickAllStoreItem(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(allStoreItem));
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(allStoreItem));
        allStoreItem.click();
    }
}
