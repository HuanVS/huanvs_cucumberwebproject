package pages;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class CustomerPage extends BasePage {
    @FindBy(id="menu-magento-customer-customer")
    private WebElement customerMenuItem;

    @FindBy(xpath = "//span[text()='All Customers']")
    private WebElement allCustomerItem;

    @FindBy(id = "add")
    private WebElement addNewCustomerBtn;

    @FindBy(name = "customer[firstname]")
    private WebElement firstName;

    @FindBy(name="customer[lastname]")
    private WebElement lastName;

    @FindBy(name = "customer[email]")
    private WebElement email;

    @FindBy(id = "save")
    private WebElement saveBtn;

    @FindBy(id="messages")
    private WebElement successMsg;

    public CustomerPage(WebDriver driver) {
        super(driver);
    }

    public void clickCustomerMenuItem(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(customerMenuItem));
        customerMenuItem.click();
    }

    public void clickAllCustomerItem(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(allCustomerItem));
        allCustomerItem.click();
    }

    public void clickAddNewCustomer(){
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(addNewCustomerBtn));
        addNewCustomerBtn.click();
    }

    public void inputInfoNewCustomer(String firstname, String lastname, String mail){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(firstName));
        firstName.sendKeys(firstname);
        lastName.sendKeys(lastname);
        email.sendKeys(mail);
    }

    public void clickSaveBtn(){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(saveBtn));
        saveBtn.click();
    }

    public boolean verifySuccessMsg(String message){
        getWebDriverWait().until(ExpectedConditions.visibilityOf(successMsg));
        return successMsg.getText().equals(message);
    }
}
