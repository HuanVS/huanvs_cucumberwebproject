package pages;

import core.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class MarketingPage extends BasePage {
    public MarketingPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "add")
    private WebElement addNewRuleBtn;

    @FindBy(id = "save")
    private WebElement saveBtn;

    @FindBy(xpath = "//label[@class='admin__field-error']")
    private List<WebElement> errorMsg;

    @FindBy(xpath = "//label[@class='admin__actions-switch-label']")
    private WebElement toggleActiveBtn;

    public void clickAddRuleBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(addNewRuleBtn));
        addNewRuleBtn.click();
    }

    public void clickSaveBtn() {
        getWebDriverWait().until(ExpectedConditions.visibilityOf(saveBtn));
        getWebDriverWait().until(ExpectedConditions.elementToBeClickable(toggleActiveBtn));
        saveBtn.click();
    }

    public boolean verifyErrorMsgDisplay(String msg) {
        getWebDriverWait().until(ExpectedConditions.visibilityOfAllElements(errorMsg));
        int sum = 0;
        for (WebElement e : errorMsg) {
            if (e.getText().equals(msg)) {
                sum++;
            }
        }
        return (sum == 4);
    }
}
