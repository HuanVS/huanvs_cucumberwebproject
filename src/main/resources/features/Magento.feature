Feature: Magento TestSuite

  Background:
    Given User open the browser for website url and login with username and password
      |url                                                                             |username|password    |
      |https://demoshops.splendid-internet.de/magento/demoshop-magento2-daily/demoadmin|demoshop|demoshop2022|

    #TC1
  Scenario: Verify Add new customer successfully
    When Click to the Customers on the left menu
    And  Click to the All Customers
    And  Click Add New Customer
    And  Input the requite field
    |firstname | lastname | email |
    |Viet      | Nam      | vietnam@gmail.com |
    And  Click to the Save Customer
    Then The message should be displayed
    |msg |
    |You saved the customer. |

    #TC2
  Scenario: Verify Product page
    When Click to the Catalog on the left menu.
    And  Click to the Products
    Then The product page should be displayed with some filed

    #TC3
  Scenario: Verify search  Product page
    When Click to the Catalog on the left menu.
    And  Click to the Products
    And  Input search key and click search
    |searchKey |
    |Joust Duffle Bag|
    Then The search result display product with name
    |name |
    |Joust Duffle Bag |

    #TC4
  Scenario: Verify required field New Catalog Price Rule
    When Click to the Marketing and choose Catalog Price Rule on the left menu.
    And  Click to Add New Rule
    And  Click Save button
    Then The message for each required field should be displayed:
    |msg |
    |This is a required field.|

    #TC5
  Scenario: Verify create new Store Information
    When Click to the Stores and choose All Stores on the left menu.
    And  Click to the Create Store.
    And  Input : Web Site, Name, Code and Root Category
    |Website | name | code | category |
    |Main Website | HuanVS | IVSAT |Default Category|
    And  Click Save
    Then Verify message should be displayed
    |msg |
    |You saved the store.|
